﻿
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SnmzCMS.WebAdmin.Filter
{
    public class CustomAuthorization : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (HttpContext.Current.Session["UserID"] == null )
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.StatusCode =
                        302;
                    filterContext.HttpContext.Response.End();
                }
                else
                {

                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary
                        {
                            { "client", filterContext.RouteData.Values[ "client" ] },
                            { "controller", "Account" },
                            { "action", "Login" },
                            { "ReturnUrl", filterContext.HttpContext.Request.RawUrl }
                        });
                }
            }
            else
            {

            }
        }
    }
}