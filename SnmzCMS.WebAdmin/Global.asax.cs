﻿using System;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using SnmzCMS.Business.Extensions;
using SnmzCMS.Business.Interceptor;
using SnmzCMS.Business.Interface;
using SnmzCMS.Business.LoggerService;
using SnmzCMS.Business.Services;
using SnmzCMS.Core;
using SnmzCMS.Data;


namespace SnmzCMS.WebAdmin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var container = new Container();            
            container.Register<IUnitOfWork<snmzEntities>, UnitOfWork<snmzEntities>>();
            container.Register<IUserService, UserService>();
            container.Register<ICategoryService, CategoryService>();
            container.Register<IFileService, FileService>();
            container.Register<IProductService, ProductService>();
            container.Register<IGalleryService, GalleryService>();
            container.Register<IContentPagesService, ContentPagesService>();
            container.Register<IWebPagesService, WebPagesService>();
            container.Register<IBannerService, BannerService>();
            container.Register<ISiteManagementService, SiteManagementService>();
            container.Register<IDocumentGalleryService, DocumentGalleryService>();
            container.Register<ISocialMediaService, SocialMediaService>();
            container.Register<ILoggerCms, LoggerCms>();
            container.Register<ServiceInterceptor>();
            

            container.InterceptWith<ServiceInterceptor>(
                serviceType => serviceType.Name.EndsWith("Service"));


            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            DependencyResolver.SetResolver(
                new SimpleInjectorDependencyResolver(container));
        }

        protected void Application_Error(object sender, EventArgs e)
        {

            
            var container = new Container();
            container.Register<IUnitOfWork<snmzEntities>, UnitOfWork<snmzEntities>>();
            container.Register<ILoggerCms, LoggerCms>();
            var logger = container.GetInstance<ILoggerCms>();

            
            var ex = Server.GetLastError();

            string action;
            if (ex.GetType() == typeof(HttpException))
            {
                var httpException = (HttpException)ex;

                switch (httpException.GetHttpCode())
                {
                    case 404:
                        action = "notFound";
                        break;
                    case 500:
                        logger.Error(ex,ex.Message, "AdminWeb ServerError");
                        action = "serverError";
                        break;
                    default:
                        logger.Error(ex, ex.Message, "AdminWeb Unexpected");
                        action = "unexpected";
                        break;
                }
            }
            else
            {
                logger.Error(ex, ex.Message, "AdminWeb Unexpected");
                action = "unexpected";
            }
            Server.ClearError();
            Response.RedirectToRoute(action);
        }
    }
}
