﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SnmzCMS.WebAdmin
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
           


            routes.MapRoute(
                name: "notFound",
                url: "sayfabulunamadi",
                defaults: new { controller = "Error", action = "NotFound" });

            routes.MapRoute(
                name: "serverError",
                url: "sunucuHatasi",
                defaults: new { controller = "Error", action = "ServerError" });

            routes.MapRoute(
                name: "unexpected",
                url: "beklenmedikHata",
                defaults: new { controller = "Error", action = "Unexpected" });

            routes.MapRoute(
                name: "login",
                url: "login",
                defaults: new { controller = "Account", action = "Login" });

            routes.MapRoute(
               name: "myprofile",
               url: "myprofile",
               defaults: new { controller = "Account", action = "MyProfile" });

            routes.MapRoute(
                name: "createCategory",
                url: "createCategory",
                defaults: new { controller = "Category", action = "DetailCategory" });

            routes.MapRoute(
                name: "detailCategory",
                url: "detailCategory/{categoryId}",
                defaults: new { controller = "Category", action = "DetailCategory" });

            routes.MapRoute(
                name: "categoryList",
                url: "categoryList",
                defaults: new { controller = "Category", action = "Index" });

            

            routes.MapRoute(
                name: "createProduct",
                url: "createProduct",
                defaults: new { controller = "Products", action = "DetailProducts" });

            routes.MapRoute(
                name: "detailProduct",
                url: "detailProduct/{productId}",
                defaults: new { controller = "Products", action = "DetailProducts" });

            routes.MapRoute(
                name: "productsList",
                url: "productsList",
                defaults: new { controller = "Products", action = "Index" });


            routes.MapRoute(
                name: "createGallery",
                url: "createGallery",
                defaults: new { controller = "Gallery", action = "DetailGallery" });

            routes.MapRoute(
                name: "detailGallery",
                url: "detailGallery/{galleryId}",
                defaults: new { controller = "Gallery", action = "DetailGallery" });

            routes.MapRoute(
                name: "galleryList",
                url: "galleryList",
                defaults: new { controller = "Gallery", action = "Index" });


            routes.MapRoute(
                name: "createWebPage",
                url: "createWebPage",
                defaults: new { controller = "WebPages", action = "DetailWebPages" });

            routes.MapRoute(
                name: "detailWebPage",
                url: "detailWebPage/{menuId}",
                defaults: new { controller = "WebPages", action = "DetailWebPages" });

            routes.MapRoute(
                name: "webPagesList",
                url: "webPagesList",
                defaults: new { controller = "WebPages", action = "Index" });

            routes.MapRoute(
                name: "createContentPage",
                url: "createContentPage",
                defaults: new { controller = "ContentPages", action = "DetailContentPage" });

            routes.MapRoute(
                name: "detailContentPage",
                url: "detailContentPage/{contentId}",
                defaults: new { controller = "ContentPages", action = "DetailContentPage" });

            routes.MapRoute(
                name: "contentPagesList",
                url: "contentPagesList",
                defaults: new { controller = "ContentPages", action = "Index" });

            routes.MapRoute(
                name: "createBanner",
                url: "createBanner",
                defaults: new { controller = "Banners", action = "DetailBanners" });

            routes.MapRoute(
                name: "detailBanner",
                url: "detailBanner/{bannerId}",
                defaults: new { controller = "Banners", action = "DetailBanners" });

            routes.MapRoute(
                name: "bannerList",
                url: "bannerList",
                defaults: new { controller = "Banners", action = "Index" });


            routes.MapRoute(
                name: "createDocumentGallery",
                url: "createDocumentGallery",
                defaults: new { controller = "DocumentGallery", action = "DetailGallery" });

            routes.MapRoute(
                name: "detailDocumentGallery",
                url: "detailDocumentGallery/{galleryId}",
                defaults: new { controller = "DocumentGallery", action = "DetailGallery" });

            routes.MapRoute(
                name: "documentGalleryList",
                url: "documentGalleryList",
                defaults: new { controller = "DocumentGallery", action = "Index" });

            routes.MapRoute(
                name: "googleAnalytics",
                url: "googleAnalytics",
                defaults: new { controller = "SiteManagement", action = "GoogleAnalytics" });

            routes.MapRoute(
                name: "googleMap",
                url: "googleMap",
                defaults: new { controller = "SiteManagement", action = "GoogleMap" });

            routes.MapRoute(
                name: "socialMedia",
                url: "socialMedia",
                defaults: new { controller = "SocialMedia", action = "Index" });

            routes.MapRoute(
                name: "createSocialMedia",
                url: "createSocialMedia",
                defaults: new { controller = "SocialMedia", action = "DetailSocialMedia" });

            routes.MapRoute(
                name: "detailSocialMedia",
                url: "detailSocialMedia/{socialMediaId}",
                defaults: new { controller = "SocialMedia", action = "DetailSocialMedia" });


            routes.MapRoute(
                name: "createUser",
                url: "createUser",
                defaults: new { controller = "Account", action = "Create" });

            routes.MapRoute(
                name: "userList",
                url: "userList",
                defaults: new { controller = "Account", action = "UserList" });

            routes.MapRoute(
                name: "home",
                url: "home",
                defaults: new { controller = "Home", action = "Index" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );
        }
    }
}
