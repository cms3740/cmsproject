﻿using System.Web.Mvc;
using SnmzCMS.Business.Interface;
using SnmzCMS.Data;

namespace SnmzCMS.WebAdmin.Controllers
{
    public class SocialMediaController : Controller
    {
        private readonly ISocialMediaService _socialMediaService;

        public SocialMediaController(ISocialMediaService socialMediaService)
        {
            _socialMediaService = socialMediaService;
        }


        public ActionResult Index()
        {
            var model = _socialMediaService.List();
            return View(model);
        }

        public ActionResult DetailSocialMedia(int? socialMediaId)
        {
            if (socialMediaId == null) return View();
            var model = _socialMediaService.GetById((int)socialMediaId);
            return View(model);
        }

        [HttpPost]
        public ActionResult DetailSocialMedia(SocialMedias model)
        {
            var result= _socialMediaService.AddOrUpdate(model);
            ViewBag.Result = result;
            return RedirectToAction("Index");
        }
    }
}