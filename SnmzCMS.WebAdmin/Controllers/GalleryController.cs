﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Helper;
using SnmzCMS.Business.Interface;
using SnmzCMS.Data;

namespace SnmzCMS.WebAdmin.Controllers
{
    public class GalleryController : Controller
    {
        private readonly IGalleryService _galleryService;        
        private readonly IFileService _fileService;

        public GalleryController( IFileService fileService, IGalleryService galleryService)
        {
            _fileService = fileService;
            _galleryService = galleryService;
        }

        public ActionResult Index()
        {
            
            var model =_galleryService.GetList((int) StatusEnum.Passive);
            return View(model);
        }

        public ActionResult DetailGallery(int? galleryId)
        {
            
            if (galleryId == null) return View();

            var model = _galleryService.GetById((int)galleryId);
            return View(model);
        }

        [HttpPost]
        public ActionResult DetailGallery(Gallery model,IEnumerable<HttpPostedFileBase> galleryUploadFiles)
        {
            
            var result = _galleryService.AddOrUpdate(model);
            ViewBag.Result = result;

            if (galleryUploadFiles == null) return RedirectToAction("Index");


            foreach (var files in galleryUploadFiles)
            {               
                if (files == null || !files.ContentType.Contains("image")) continue;

                var fileModel = UploadHelper.UploadFile(Request.PhysicalApplicationPath, ConfigurationManager.AppSettings["ImageDirectory"], files, FileTypeEnum.Photo);

                _fileService.SaveFile(fileModel);

                var productFiles = new GalleryFiles
                {
                    GalleryId = model.Id,
                    FileId = fileModel.Id
                };
                _galleryService.AddGalleryFiles(productFiles);
            }         
            return RedirectToAction("Index");
        }
       
    }
}