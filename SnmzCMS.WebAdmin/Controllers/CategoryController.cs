﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Helper;
using SnmzCMS.Business.Interface;
using SnmzCMS.Data;

namespace SnmzCMS.WebAdmin.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly IFileService _fileService;

        public CategoryController(ICategoryService categoryService, IFileService fileService)
        {
            _categoryService = categoryService;
            _fileService = fileService;
        }

        public ActionResult Index()
        {
            var model =_categoryService.GetList((int) StatusEnum.Passive);
            return View(model);
        }

        public ActionResult DetailCategory(int? categoryId)
        {
            if (categoryId == null) return View();
            var model = _categoryService.GetById((int)categoryId);
            return View(model);
        }

        [HttpPost]
        public ActionResult DetailCategory(Category model,HttpPostedFileBase categoryUploadFile)
        {
            if (categoryUploadFile != null && categoryUploadFile.ContentType.Contains("image"))
            {               

                var fileModel = UploadHelper.UploadFile(Request.PhysicalApplicationPath, ConfigurationManager.AppSettings["ImageDirectory"], categoryUploadFile, FileTypeEnum.Photo);

                _fileService.SaveFile(fileModel);

                model.FileId = fileModel.Id;
            }

            var result= _categoryService.AddOrUpdate(model);
            ViewBag.Result = result;
            return RedirectToAction("Index");
        }
    }
}