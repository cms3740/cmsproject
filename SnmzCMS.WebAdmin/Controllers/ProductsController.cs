﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Helper;
using SnmzCMS.Business.Interface;
using SnmzCMS.Data;

namespace SnmzCMS.WebAdmin.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IFileService _fileService;

        public ProductsController( IFileService fileService, IProductService productService, ICategoryService categoryService)
        {
            _fileService = fileService;
            _productService = productService;
            _categoryService = categoryService;
        }

        public ActionResult Index()
        {
            
            var model =_productService.GetList((int) StatusEnum.Passive);
            return View(model);
        }

        public ActionResult DetailProducts(int? productId)
        {
            ViewBag.CategoryList = _categoryService.GetList((int)StatusEnum.Active);
            if (productId == null) return View();

            var model = _productService.GetById((int)productId);
            return View(model);
        }

        [HttpPost,ValidateInput(false)]
        public ActionResult DetailProducts(Products model,IEnumerable<HttpPostedFileBase> productUploadFiles)
        {
           
            var result = _productService.AddOrUpdate(model);
            ViewBag.Result = result;

            ViewBag.CategoryList = _categoryService.GetList((int)StatusEnum.Active);

            if (productUploadFiles == null) return RedirectToAction("Index");


            foreach (var files in productUploadFiles)
            {               
                if (files == null || !files.ContentType.Contains("image")) continue;

                var fileModel = UploadHelper.UploadFile(Request.PhysicalApplicationPath, ConfigurationManager.AppSettings["ImageDirectory"], files, FileTypeEnum.Photo);

                _fileService.SaveFile(fileModel);

                var productFiles = new ProductFiles
                {
                    ProductId = model.Id,
                    FileId = fileModel.Id
                };
                _productService.AddProductFiles(productFiles);
            }         
            return RedirectToAction("Index");
        }
       
    }
}