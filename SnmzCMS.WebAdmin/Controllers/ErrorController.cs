﻿using System.Web.Mvc;

namespace SnmzCMS.WebAdmin.Controllers
{
    public class ErrorController : Controller
    {        
        public ActionResult NotFound()
        {
            return View();
        }
        public ActionResult ServerError()
        {
            return View();
        }
        public ActionResult Unexpected()
        {
            return View();
        }
    }
}