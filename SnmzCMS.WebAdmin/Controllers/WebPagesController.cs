﻿using System.Linq;
using System.Web.Mvc;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Interface;
using SnmzCMS.Data;

namespace SnmzCMS.WebAdmin.Controllers
{
    public class WebPagesController : Controller
    {
        private readonly IWebPagesService _webPagesService;

        public WebPagesController(IWebPagesService webPagesService)
        {
            _webPagesService = webPagesService;
        }


        public ActionResult Index()
        {
            
            var model =_webPagesService.GetList((int) StatusEnum.Passive);
            return View(model);
        }

        public ActionResult DetailWebPages(int? menuId)
        {
            ViewBag.TopMenuList = _webPagesService.GetList((int) StatusEnum.Active).Where(x => x.ParentId == null);


            if (menuId == null) return View();

            var model = _webPagesService.GetById((int)menuId);
            return View(model);
        }

        [HttpPost]
        public ActionResult DetailWebPages(WebPages model)
        {            
            var result = _webPagesService.AddOrUpdate(model);
            ViewBag.Result = result;
            ViewBag.TopMenuList = _webPagesService.GetList((int)StatusEnum.Active).Where(x => x.ParentId == null);
            return RedirectToAction("Index");
        }
       
    }
}