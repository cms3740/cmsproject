﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Helper;
using SnmzCMS.Business.Interface;
using SnmzCMS.Data;

namespace SnmzCMS.WebAdmin.Controllers
{
    public class ContentPagesController : Controller
    {
        private readonly IContentPagesService _contentPagesService;
        private readonly IWebPagesService _webPagesService;
        private readonly IGalleryService _galleryService;
        private readonly IFileService _fileService;

        public ContentPagesController(IContentPagesService contentPagesService,IWebPagesService webPagesService, IGalleryService galleryService, IFileService fileService)
        {
            _contentPagesService = contentPagesService;
            _webPagesService = webPagesService;
            _galleryService = galleryService;
            _fileService = fileService;
        }

        public ActionResult Index()
        {
            var model =_contentPagesService.GetList((int) StatusEnum.Passive);
            return View(model);
        }

        public ActionResult DetailContentPage(int? contentId)
        {
            ViewBag.WebPages = _webPagesService.GetList((int)StatusEnum.Active);
            ViewBag.Gallery = _galleryService.GetList((int)StatusEnum.Active);
            if (contentId == null) return View();
            var model = _contentPagesService.GetById((int)contentId);           
            return View(model);
        }

        [HttpPost,ValidateInput(false)]
        public ActionResult DetailContentPage(ContentPages model, HttpPostedFileBase contentUploadFile)
        {

            if (contentUploadFile != null && contentUploadFile.ContentType.Contains("image"))
            {

                var fileModel = UploadHelper.UploadFile(Request.PhysicalApplicationPath, ConfigurationManager.AppSettings["ImageDirectory"], contentUploadFile, FileTypeEnum.Photo);

                _fileService.SaveFile(fileModel);
                model.FileId = fileModel.Id;
            }

            var result= _contentPagesService.AddOrUpdate(model);
            ViewBag.Result = result;
            ViewBag.WebPages = _webPagesService.GetList((int) StatusEnum.Active);
            ViewBag.Gallery = _galleryService.GetList((int) StatusEnum.Active);
                
            return RedirectToAction("Index");
        }
    }
}