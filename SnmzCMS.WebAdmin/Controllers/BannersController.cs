﻿using System.Configuration;
using System.Web;
using System.Web.Mvc;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Helper;
using SnmzCMS.Business.Interface;
using SnmzCMS.Data;

namespace SnmzCMS.WebAdmin.Controllers
{
    public class BannersController : Controller
    {
        private readonly IBannerService _bannerService;        
        private readonly IFileService _fileService;

        public BannersController( IFileService fileService, IBannerService bannerService)
        {
            _fileService = fileService;
            _bannerService = bannerService;
        }

        public ActionResult Index()
        {
            
            var model =_bannerService.GetList((int) StatusEnum.Passive);
            return View(model);
        }

        public ActionResult DetailBanners(int? bannerId)
        {           
            if (bannerId == null) return View();

            var model = _bannerService.GetById((int)bannerId);
            return View(model);
        }

        [HttpPost,ValidateInput(false)]
        public ActionResult DetailBanners(Banners model,HttpPostedFileBase bannerUploadFiles)
        {

            if (bannerUploadFiles != null && bannerUploadFiles.ContentType.Contains("image"))
            {
                var fileModel = UploadHelper.UploadFile(Request.PhysicalApplicationPath, ConfigurationManager.AppSettings["ImageDirectory"], bannerUploadFiles, FileTypeEnum.Photo);
                _fileService.SaveFile(fileModel);
                model.FileId = fileModel.Id;
            }

            var result = _bannerService.AddOrUpdate(model);
            ViewBag.Result = result;
            return RedirectToAction("Index");
        }
       
    }
}