﻿using System;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Helper;
using SnmzCMS.Business.Interface;
using SnmzCMS.Data;
using SnmzCMS.Data.ViewModel;
using SnmzCMS.WebAdmin.Filter;

namespace SnmzCMS.WebAdmin.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {

            string oldHashValue = string.Empty;
            byte[] salt = new byte[16];


            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                var userInfo = _userService.List((int)StatusEnum.Active).FirstOrDefault(s => s.UserName == model.UserName.Trim());

                if (userInfo != null)
                {
                    oldHashValue = userInfo.Hash;
                    salt = userInfo.Salt;
                }

                bool isLogin = CompareHashValueHelper.CompareHashValue(model.UserName, model.Password, oldHashValue, salt);

                if (userInfo != null && isLogin && userInfo.RoleId == (int)RoleEnum.Admin && userInfo.StatusId == (int)StatusEnum.Active)
                {

                    SignInRemember(model.UserName, model.RememberMe);
                    Session["UserID"] = userInfo.Id;                    
                    HttpCookie cookie = new HttpCookie("userInfo");
                    cookie.Values.Add("Name", userInfo.Name);
                    cookie.Values.Add("LastName", userInfo.SurName);
                    cookie.Expires = DateTime.Now.AddDays(30);
                    Response.Cookies.Add(cookie);

                    return RedirectToLocal(model.ReturnUrl);
                }
                else
                {
                    TempData["ErrorMsg"] = "Giriş Başarısız. Bilgilerinizi kontrol edin.";
                    return View(model);
                }
            }
        }

        private void SignInRemember(string userName, bool isPersistent = false)
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.SetAuthCookie(userName, isPersistent);
        }

        private ActionResult RedirectToLocal(string returnUrl = "")
        {

            if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);


            return RedirectToAction("Index", "Home");

        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Create(Users model,FormCollection form)
        {
            var user = new Users
            {
                Name = model.Name,
                SurName = model.SurName,
                UserName = model.UserName,
                RoleId = (int)RoleEnum.Admin,
                StatusId = model.StatusId = model.StatusId ?? (int)StatusEnum.Passive,
                Salt = Generator.Get_SALT()
            };

            user.Hash = Generator.Get_HASH_SHA512(user.UserName, form["password"], user.Salt);
            _userService.Create(user);

            return RedirectToAction("UserList", "Account");
        }

        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);

            Session.Clear();
            System.Web.HttpContext.Current.Session.RemoveAll();

            if (Request.Cookies["userInfo"] == null) return RedirectToAction("Login", "Account");

            var c = new HttpCookie("userInfo") {Expires = DateTime.Now.AddDays(-1)};
            Response.Cookies.Add(c);
            return RedirectToAction("Login", "Account");

        }

        [CustomAuthorization]
        public ActionResult UserList()
        {                      
            var model = _userService.List((int)StatusEnum.Passive);
            return View(model);
        }


        [CustomAuthorization]
        public ActionResult MyProfile()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Account");

            var userId = (int)Session["UserID"];
            var model = _userService.GetById(userId);
            return View(model);
        }

        [HttpPost]
        public ActionResult MyProfile(Users model)
        {
            var oldModel = _userService.GetById(model.Id);
            oldModel.Name = model.Name;
            oldModel.SurName = model.SurName;
            oldModel.Phone = model.Phone;
            oldModel.UserName = model.UserName;

            var result =_userService.Update(oldModel);
            if(result.Success)
                ViewBag.Result=result;

            return View(model);
        }

    }
}