﻿using System.Web.Mvc;
using SnmzCMS.Business.Interface;
using SnmzCMS.Data;

namespace SnmzCMS.WebAdmin.Controllers
{
    public class SiteManagementController : Controller
    {
        private readonly ISiteManagementService _siteManagementService;

        public SiteManagementController(ISiteManagementService siteManagementService)
        {
            _siteManagementService = siteManagementService;
        }
       
        public ActionResult GoogleAnalytics()
        {
            var model = _siteManagementService.GetByName("analytics");
            return model != null ? View(model) : View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GoogleAnalytics(SiteManagement model)
        {
            var result= _siteManagementService.AddOrUpdate(model);
            if (result.Success)
                return RedirectToAction("GoogleAnalytics", "SiteManagement");
            return View(model);
        }


        public ActionResult GoogleMap()
        {
            var model = _siteManagementService.GetByName("map");
            return model != null ? View(model) : View();
        }

        [HttpPost,ValidateInput(false)]
        public ActionResult GoogleMap(SiteManagement model)
        {
            var result=_siteManagementService.AddOrUpdate(model);
            if (result.Success)
                return RedirectToAction("GoogleMap", "SiteManagement");
            return View(model);
        }
    }
}