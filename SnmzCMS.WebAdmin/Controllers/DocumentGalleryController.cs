﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Helper;
using SnmzCMS.Business.Interface;
using SnmzCMS.Data;

namespace SnmzCMS.WebAdmin.Controllers
{
    public class DocumentGalleryController : Controller
    {
        private readonly IDocumentGalleryService _documentGalleryService;        
        private readonly IFileService _fileService;

        public DocumentGalleryController( IFileService fileService, IDocumentGalleryService documentGalleryService)
        {
            _fileService = fileService;
            _documentGalleryService = documentGalleryService;
        }

        public ActionResult Index()
        {
            
            var model = _documentGalleryService.GetList((int) StatusEnum.Passive);
            return View(model);
        }

        public ActionResult DetailGallery(int? galleryId)
        {
            
            if (galleryId == null) return View();

            var model = _documentGalleryService.GetById((int)galleryId);
            return View(model);
        }

        [HttpPost]
        public ActionResult DetailGallery(DocumentGallery model,IEnumerable<HttpPostedFileBase> galleryUploadFiles)
        {
            
            var result = _documentGalleryService.AddOrUpdate(model);
            ViewBag.Result = result;

            if (galleryUploadFiles == null) return RedirectToAction("Index");


            foreach (var files in galleryUploadFiles)
            {               
                
                var fileModel = UploadHelper.UploadFile(Request.PhysicalApplicationPath, ConfigurationManager.AppSettings["ImageDirectory"], files, FileTypeEnum.Document);

                _fileService.SaveFile(fileModel);

                var galleryFiles = new DocumentGalleryFiles
                {
                    DocumentGalleryId = model.Id,
                    FileId = fileModel.Id
                };
                _documentGalleryService.AddDocumentGalleryFiles(galleryFiles);
            }         
            return RedirectToAction("Index");
        }
       
    }
}