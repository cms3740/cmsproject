﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnmzCMS.Business.Enums
{
    public enum FileTypeEnum
    {
        Photo =1,
        Document =2,
        Sound =3
    }
}
