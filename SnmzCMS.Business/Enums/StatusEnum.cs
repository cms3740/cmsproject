﻿
namespace SnmzCMS.Business.Enums
{
    public enum StatusEnum
    {
        Passive =0,
        Active = 1,
        Deleted = 255
    }
}
