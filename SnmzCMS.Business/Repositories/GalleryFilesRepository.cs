﻿using SnmzCMS.Core;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Repositories
{
    public class GalleryFilesRepository : GRepository<GalleryFiles, snmzEntities>
    {
        public GalleryFilesRepository(IUnitOfWork<snmzEntities> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
