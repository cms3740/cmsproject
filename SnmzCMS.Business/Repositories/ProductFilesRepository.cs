﻿using SnmzCMS.Core;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Repositories
{
    public class ProductFilesRepository : GRepository<ProductFiles, snmzEntities>
    {
        public ProductFilesRepository(IUnitOfWork<snmzEntities> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
