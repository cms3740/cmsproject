﻿using SnmzCMS.Core;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Repositories
{
    public class DocumentGalleryRepository : GRepository<DocumentGallery, snmzEntities>
    {
        public DocumentGalleryRepository(IUnitOfWork<snmzEntities> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
