﻿using SnmzCMS.Core;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Repositories
{
    public class BannerRepository : GRepository<Banners, snmzEntities>
    {
        public BannerRepository(IUnitOfWork<snmzEntities> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
