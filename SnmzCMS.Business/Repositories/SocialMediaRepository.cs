﻿using SnmzCMS.Core;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Repositories
{
    public class SocialMediaRepository : GRepository<SocialMedias, snmzEntities>
    {
        public SocialMediaRepository(IUnitOfWork<snmzEntities> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
