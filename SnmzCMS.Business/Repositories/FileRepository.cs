﻿using SnmzCMS.Core;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Repositories
{
    public class FileRepository : GRepository<Files, snmzEntities>
    {
        public FileRepository(IUnitOfWork<snmzEntities> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
