﻿using SnmzCMS.Core;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Repositories
{
    public class DocumentGalleryFilesRepository : GRepository<DocumentGalleryFiles, snmzEntities>
    {
        public DocumentGalleryFilesRepository(IUnitOfWork<snmzEntities> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
