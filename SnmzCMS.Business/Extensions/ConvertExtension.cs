﻿using System;
using System.Globalization;
using System.Security.Cryptography;

namespace SnmzCMS.Business.Extensions
{
    public static class ConvertExtension
    {
        public static int ToInt(this object str)
        {
            return Convert.ToInt32(str);
        }
        public static decimal ToDecimal(this object str)
        {
            return Convert.ToDecimal(str);
        }
        public static bool ToBoolean(this object str)
        {
            return Convert.ToBoolean(str);
        }
        public static byte ToByte(this object str)
        {
            return Convert.ToByte(str);
        }
        public static DateTime ToDateTime(this object str)
        {
            return Convert.ToDateTime(str, new CultureInfo("tr-TR"));
        }
        public static string ToMd5(this String text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] bytes = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(text));
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            foreach (byte b in bytes)
            {
                builder.Append(b.ToString("x2").ToLower());
            }
            return builder.ToString();
        }

        public static string ToRelativeTime(this DateTime dateTime)
        {
            DateTime when = dateTime;
            TimeSpan ts = DateTime.Now.Subtract(when);
            if (ts.TotalHours < 1)
            {
                return ts.TotalMinutes.ToInt().ToString() + " dakika önce";
            }
            else if (ts.TotalDays < 1)
            {
                return ts.TotalHours.ToInt().ToString() + " saat önce";
            }
            else if (ts.TotalMinutes < 1)
            {
                return ts.TotalSeconds.ToInt().ToString() + " saniye önce";
            }
            else if (ts.TotalHours > 24)
            {
                return ts.TotalDays.ToInt().ToString() + " gün önce";
            }
            else
            {
                return "";
            }
        }
    }
}
