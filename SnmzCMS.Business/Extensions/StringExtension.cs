﻿using System.Text.RegularExpressions;

namespace SnmzCMS.Business.Extensions
{
    public static class StringExtension
    {
        public static string StripHtml(this string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                string result = Regex.Replace(input, "<.*?>| &.*?;", string.Empty);
                return result;
            }
            return input;
        }

        public static string GetSummary(this string input, int lenght)
        {
            if (input != null)
            {
                if (input.Length > lenght)
                {
                    return input.Substring(0, lenght);
                }
                return input;
            }
            return null;
        }
    }
}
