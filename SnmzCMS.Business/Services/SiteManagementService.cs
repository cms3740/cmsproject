﻿using System.Collections.Generic;
using System.Linq;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Interface;
using SnmzCMS.Business.Repositories;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Services
{
    public class SiteManagementService : BaseService, ISiteManagementService
    {
        private readonly SiteManagementRepository _siteManagementRepository;

        public SiteManagementService(SiteManagementRepository siteManagementRepository)
        {
            _siteManagementRepository = siteManagementRepository;
        }


        public List<SiteManagement> List()
        {
            
            return _siteManagementRepository.Source.Where(x => x.StatusId == (int)StatusEnum.Active).ToList();            
        }

        public ResultModel AddOrUpdate(SiteManagement model)
        {
            model.StatusId = model.StatusId ?? (int)StatusEnum.Passive;

            if (model.Id > 0)
                _siteManagementRepository.Update(model);
            else
                _siteManagementRepository.Insert(model);

            var result = _siteManagementRepository.Commit();
            result.Message = result.Success ? "Başarılı" : "Hata Var. Daha Sonra Tekrar Deneyiniz!";

            return result;
        }

       

        public ResultModel Delete(int id)
        {
            var model = _siteManagementRepository.GetById(id);
            model.StatusId = (int)StatusEnum.Deleted;
            _siteManagementRepository.Update(model);
            return _siteManagementRepository.Commit();
        }

        public SiteManagement GetById(int id)
        {
            return _siteManagementRepository.GetById(id);
        }

        public SiteManagement GetByName(string name)
        {
            return _siteManagementRepository.Source.FirstOrDefault(x => x.Name == name);
        }
    }
}
