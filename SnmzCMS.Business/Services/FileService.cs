﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Interface;
using SnmzCMS.Business.Repositories;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Services
{
    public class FileService : BaseService, IFileService
    {
        private readonly FileRepository _fileRepository;

        public FileService(FileRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }


        public ResultModel SaveFile(Files model)
        {
            _fileRepository.Insert(model);
            return _fileRepository.Commit();
        }
    }
}
