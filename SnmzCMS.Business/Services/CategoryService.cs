﻿using System.Collections.Generic;
using System.Linq;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Interface;
using SnmzCMS.Business.Repositories;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Services
{
    public class CategoryService : BaseService, ICategoryService
    {
        private readonly CategoryRepository _categoryRepository;

        public CategoryService(CategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }


        public ResultModel AddOrUpdate(Category model)
        {
            model.StatusId = model.StatusId ?? (int) StatusEnum.Passive;

            if(model.Id > 0) 
                _categoryRepository.Update(model);
            else
                _categoryRepository.Insert(model);

            var result= _categoryRepository.Commit();
            result.Message = result.Success ? "Başarılı" : "Hata Var. Daha Sonra Tekrar Deneyiniz!";

            return result;
        }

        public ResultModel Delete(int categoryId)
        {
            var oldModel=_categoryRepository.GetById(categoryId);
            oldModel.StatusId = (int) StatusEnum.Deleted;
            _categoryRepository.Update(oldModel);
            return _categoryRepository.Commit();
        }

       

        public List<Category> GetList(int statusId)
        {
            var model = _categoryRepository.GetList();
            return statusId == (int) StatusEnum.Active
                ? model.Where(x => x.StatusId == (int) StatusEnum.Active).ToList()
                : model;
        }

        public Category GetById(int categoryId)
        {
            return _categoryRepository.GetById(categoryId);
        }
    }
}
