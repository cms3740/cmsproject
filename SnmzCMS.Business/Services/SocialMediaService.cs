﻿using System.Collections.Generic;
using System.Linq;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Interface;
using SnmzCMS.Business.Repositories;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Services
{
    public class SocialMediaService : BaseService, ISocialMediaService
    {
        private readonly SocialMediaRepository _socialMediaRepository;

        public SocialMediaService(SocialMediaRepository socialMediaRepository)
        {
            _socialMediaRepository = socialMediaRepository;
        }


        public List<SocialMedias> List()
        {
            
            return _socialMediaRepository.Source.Where(x => x.StatusId == (int)StatusEnum.Active).ToList();            
        }

        public ResultModel AddOrUpdate(SocialMedias model)
        {
            model.StatusId = model.StatusId ?? (int)StatusEnum.Passive;

            if (model.Id > 0)
                _socialMediaRepository.Update(model);
            else
                _socialMediaRepository.Insert(model);

            var result = _socialMediaRepository.Commit();
            result.Message = result.Success ? "Başarılı" : "Hata Var. Daha Sonra Tekrar Deneyiniz!";

            return result;
        }

        public ResultModel Delete(int id)
        {
            var model = _socialMediaRepository.GetById(id);
            model.StatusId = (int)StatusEnum.Deleted;
            _socialMediaRepository.Update(model);
            return _socialMediaRepository.Commit();
        }

        public SocialMedias GetById(int id)
        {
            return _socialMediaRepository.GetById(id);
        }    
    }
}
