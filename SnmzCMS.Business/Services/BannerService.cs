﻿using System.Collections.Generic;
using System.Linq;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Interface;
using SnmzCMS.Business.Repositories;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Services
{
    public class BannerService : BaseService, IBannerService
    {
        private readonly BannerRepository _bannerRepository;

        public BannerService(BannerRepository bannerRepository)
        {
            _bannerRepository = bannerRepository;
        }


        public ResultModel AddOrUpdate(Banners model)
        {
            model.StatusId = model.StatusId ?? (int) StatusEnum.Passive;

            if(model.Id > 0)
                _bannerRepository.Update(model);
            else
                _bannerRepository.Insert(model);

            var result= _bannerRepository.Commit();
            result.Message = result.Success ? "Başarılı" : "Hata Var. Daha Sonra Tekrar Deneyiniz!";

            return result;
        }

        public ResultModel Delete(int bannerId)
        {
            var oldModel= _bannerRepository.GetById(bannerId);
            oldModel.StatusId = (int) StatusEnum.Deleted;
            _bannerRepository.Update(oldModel);
            return _bannerRepository.Commit();
        }

       

        public List<Banners> GetList(int statusId)
        {
            var model = _bannerRepository.GetList();
            return statusId == (int) StatusEnum.Active
                ? model.Where(x => x.StatusId == (int) StatusEnum.Active).ToList()
                : model;
        }

        public Banners GetById(int productId)
        {
            return _bannerRepository.GetById(productId);
        }               
    }
}
