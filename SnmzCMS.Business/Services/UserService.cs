﻿using System;
using System.Collections.Generic;
using System.Linq;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Interface;
using SnmzCMS.Business.Repositories;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly UserRepository _userRepository;

        public UserService(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public List<Users> List(int statusId)
        {
            var model = _userRepository.GetList();
            return statusId == (int)StatusEnum.Active
                ? model.Where(x => x.StatusId == (int)StatusEnum.Active).ToList()
                : model;
                   
        }

        public ResultModel Create(Users user)
        {
            _userRepository.Insert(user);
           return _userRepository.Commit();            
        }

        public ResultModel Update(Users user)
        {
            _userRepository.Update(user);
           return _userRepository.Commit();
        }

        public ResultModel Delete(int id)
        {
            var model = _userRepository.GetById(id);
            model.StatusId = (int)StatusEnum.Deleted;
            _userRepository.Update(model);
            return _userRepository.Commit();
        }

        public Users GetById(int id)
        {
            return _userRepository.GetById(id);
        }
    }
}
