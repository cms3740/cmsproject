﻿using System.Collections.Generic;
using System.Linq;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Interface;
using SnmzCMS.Business.Repositories;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Services
{
    public class DocumentGalleryService : BaseService, IDocumentGalleryService
    {
        private readonly DocumentGalleryRepository _galleryRepository;
        private readonly DocumentGalleryFilesRepository _galleryFilesRepository;

        public DocumentGalleryService(DocumentGalleryFilesRepository galleryFilesRepository, DocumentGalleryRepository galleryRepository)
        {
            _galleryFilesRepository = galleryFilesRepository;
            _galleryRepository = galleryRepository;
        }


        public ResultModel AddOrUpdate(DocumentGallery model)
        {
            model.StatusId = model.StatusId ?? (int) StatusEnum.Passive;

            if(model.Id > 0) 
                _galleryRepository.Update(model);
            else
                _galleryRepository.Insert(model);

            var result= _galleryRepository.Commit();
            result.Message = result.Success ? "Başarılı" : "Hata Var. Daha Sonra Tekrar Deneyiniz!";

            return result;
        }

        public ResultModel Delete(int documentGalleryId)
        {
            var oldModel= _galleryRepository.GetById(documentGalleryId);
            oldModel.StatusId = (int) StatusEnum.Deleted;
            _galleryRepository.Update(oldModel);
            return _galleryRepository.Commit();
        }

       

        public List<DocumentGallery> GetList(int statusId)
        {
            var model = _galleryRepository.GetList();
            return statusId == (int) StatusEnum.Active
                ? model.Where(x => x.StatusId == (int) StatusEnum.Active).ToList()
                : model;
        }

        public DocumentGallery GetById(int documentGalleryId)
        {
            return _galleryRepository.GetById(documentGalleryId);
        }

       


        public ResultModel AddDocumentGalleryFiles(DocumentGalleryFiles model)
        {
            if (model.Id > 0)
                _galleryFilesRepository.Update(model);
            else
                _galleryFilesRepository.Insert(model);

            var result = _galleryFilesRepository.Commit();
            result.Message = result.Success ? "Başarılı" : "Hata Var. Daha Sonra Tekrar Deneyiniz!";

            return result;
        }
    }
}
