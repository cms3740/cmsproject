﻿using System.Collections.Generic;
using System.Linq;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Interface;
using SnmzCMS.Business.Repositories;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Services
{
    public class WebPagesService : BaseService, IWebPagesService
    {
        private readonly WebPagesRepository _webPagesRepository;

        public WebPagesService(WebPagesRepository webPagesRepository)
        {
            _webPagesRepository = webPagesRepository;
        }


        public ResultModel AddOrUpdate(WebPages model)
        {
            model.StatusId = model.StatusId ?? (int) StatusEnum.Passive;

            if(model.Id > 0) 
                _webPagesRepository.Update(model);
            else
                _webPagesRepository.Insert(model);

            var result= _webPagesRepository.Commit();
            result.Message = result.Success ? "Başarılı" : "Hata Var. Daha Sonra Tekrar Deneyiniz!";

            return result;
        }

        public ResultModel Delete(int menuId)
        {
            var oldModel= _webPagesRepository.GetById(menuId);
            oldModel.StatusId = (int) StatusEnum.Deleted;
            _webPagesRepository.Update(oldModel);
            return _webPagesRepository.Commit();
        }

        public List<WebPages> GetList(int statusId)
        {
            var model = _webPagesRepository.GetList();
            return statusId == (int) StatusEnum.Active
                ? model.Where(x => x.StatusId == (int) StatusEnum.Active).ToList()
                : model;
        }

        public WebPages GetById(int productId)
        {
            return _webPagesRepository.GetById(productId);
        }

        
    }
}
