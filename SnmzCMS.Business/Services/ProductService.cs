﻿using System.Collections.Generic;
using System.Linq;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Interface;
using SnmzCMS.Business.Repositories;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Services
{
    public class ProductService : BaseService, IProductService
    {
        private readonly ProductRepository _productRepository;
        private readonly ProductFilesRepository _productFilesRepository;

        public ProductService(ProductRepository productRepository, ProductFilesRepository productFilesRepository)
        {
            _productRepository = productRepository;
            _productFilesRepository = productFilesRepository;
        }


        public ResultModel AddOrUpdate(Products model)
        {
            model.StatusId = model.StatusId ?? (int) StatusEnum.Passive;

            if(model.Id > 0) 
                _productRepository.Update(model);
            else
                _productRepository.Insert(model);

            var result= _productRepository.Commit();
            result.Message = result.Success ? "Başarılı" : "Hata Var. Daha Sonra Tekrar Deneyiniz!";

            return result;
        }

        public ResultModel Delete(int productId)
        {
            var oldModel=_productRepository.GetById(productId);
            oldModel.StatusId = (int) StatusEnum.Deleted;
            _productRepository.Update(oldModel);
            return _productRepository.Commit();
        }

       

        public List<Products> GetList(int statusId)
        {
            var model = _productRepository.GetList();
            return statusId == (int) StatusEnum.Active
                ? model.Where(x => x.StatusId == (int) StatusEnum.Active).ToList()
                : model;
        }

        public Products GetById(int productId)
        {
            return _productRepository.GetById(productId);
        }

        public ResultModel AddProductFiles(ProductFiles model)
        {
            if (model.Id > 0)
                _productFilesRepository.Update(model);
            else
                _productFilesRepository.Insert(model);

            var result = _productFilesRepository.Commit();
            result.Message = result.Success ? "Başarılı" : "Hata Var. Daha Sonra Tekrar Deneyiniz!";

            return result;
        }
    }
}
