﻿using System.Collections.Generic;
using System.Linq;
using SnmzCMS.Business.Enums;
using SnmzCMS.Business.Interface;
using SnmzCMS.Business.Repositories;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Services
{
    public class ContentPagesService : BaseService, IContentPagesService
    {
        private readonly ContentPagesRepository _contentPagesRepository;

        public ContentPagesService(ContentPagesRepository contentPagesRepository)
        {
            _contentPagesRepository = contentPagesRepository;
        }


        public ResultModel AddOrUpdate(ContentPages model)
        {
            model.StatusId = model.StatusId ?? (int) StatusEnum.Passive;

            if(model.Id > 0) 
                _contentPagesRepository.Update(model);
            else
                _contentPagesRepository.Insert(model);

            var result= _contentPagesRepository.Commit();
            result.Message = result.Success ? "Başarılı" : "Hata Var. Daha Sonra Tekrar Deneyiniz!";

            return result;
        }

        public ResultModel Delete(int contentId)
        {
            var oldModel= _contentPagesRepository.GetById(contentId);
            oldModel.StatusId = (int) StatusEnum.Deleted;
            _contentPagesRepository.Update(oldModel);
            return _contentPagesRepository.Commit();
        }

        public List<ContentPages> GetList(int statusId)
        {
            var model = _contentPagesRepository.GetList();
            return statusId == (int) StatusEnum.Active
                ? model.Where(x => x.StatusId == (int) StatusEnum.Active).ToList()
                : model;
        }

        public ContentPages GetById(int productId)
        {
            return _contentPagesRepository.GetById(productId);
        }

        
    }
}
