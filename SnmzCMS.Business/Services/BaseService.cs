﻿using SnmzCMS.Business.Interface;
using SnmzCMS.Core.ViewModel;

namespace SnmzCMS.Business.Services
{
    public class BaseService : IBaseService
    {
        protected BaseService()
        {
            Result = new ResultModel();
        }
        public ResultModel Result { get; set; }
    }
}
