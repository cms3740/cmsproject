﻿using System;
using SnmzCMS.Business.LoggerService;


namespace SnmzCMS.Business.Interceptor
{
    public class ServiceInterceptor : IInterceptor
    {
        private readonly ILoggerCms _logger;

        public ServiceInterceptor(ILoggerCms logger)
        {
            _logger = logger;
        }

        public void Intercept(IInvocation invocation)
        {

            try
            {
                invocation.Proceed();
            }
            catch (Exception e)
            {
                _logger.Error(e,e.Message,"Service");                
            }
            // Calls the decorated instance.

            //var decoratedType = invocation.InvocationTarget.GetType();WW            
        }
    }
}
