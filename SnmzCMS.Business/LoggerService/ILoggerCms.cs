﻿using System;


namespace SnmzCMS.Business.LoggerService
{
    public interface ILoggerCms
    {
        void Error(Exception x,string message,string placeOfError);
    }
}
