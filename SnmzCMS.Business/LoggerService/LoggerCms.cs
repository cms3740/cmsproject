﻿using System;
using SnmzCMS.Business.Helper;
using SnmzCMS.Business.Repositories;
using SnmzCMS.Business.Services;
using SnmzCMS.Data;

namespace SnmzCMS.Business.LoggerService
{
    public class LoggerCms : BaseService, ILoggerCms
    {
        private readonly LogRepository _logRepository;

      
        public LoggerCms(LogRepository logRepository)
        {
            _logRepository = logRepository;
        }

        public void Error(Exception x, string message, string placeOfError)
        {
            var logCms = new LogCms
            {
                Created = DateTime.Now,
                ExceptionMessage = x.ToString(),
                Message = message,
                PlaceOfError = placeOfError
            };

            _logRepository.Insert(logCms);
            _logRepository.Commit();


            if (placeOfError == "Service")
            {
                //TODO: burdayı daha sonra DB den alacağız. Kime mail atılmasını istendiğini.
                MailHelper.SendMail("ahmet.sonmez37@gmail.com", x.ToString(), x.Message, "CMS-Admin Error");
            }
        }
    }
}
