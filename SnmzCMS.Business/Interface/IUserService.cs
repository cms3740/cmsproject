﻿using System.Collections.Generic;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Interface
{
    public interface IUserService
    {
        /// <summary>
        /// Tüm Kullanıcıları Listeler (web ve admin dahil)
        /// </summary>
        /// <returns></returns>
        List<Users> List(int statusId);

        /// <summary>
        /// Admin Kullanıcısı Register Eder.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        ResultModel Create(Users user);

        /// <summary>
        /// Admin Kullanıcısını Günceller
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        ResultModel Update(Users user);

        /// <summary>
        /// Admin Kullanıcısnın StatusId sini 255 yapar.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResultModel Delete(int id);

        /// <summary>
        /// UserId e ait kullanıcı bilgilerini verir.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Users GetById(int id);
    }
}
