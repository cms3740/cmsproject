﻿using System.Collections.Generic;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Interface
{
    public interface IContentPagesService
    {
        ResultModel AddOrUpdate(ContentPages model);
        ResultModel Delete(int contentId);       
        List<ContentPages> GetList(int statusId);
        ContentPages GetById(int contentId);        
    }
}
