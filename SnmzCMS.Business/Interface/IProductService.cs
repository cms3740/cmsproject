﻿using System.Collections.Generic;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Interface
{
    public interface IProductService
    {
        ResultModel AddOrUpdate(Products model);
        ResultModel Delete(int productId);       
        List<Products> GetList(int statusId);
        Products GetById(int productId);
        ResultModel AddProductFiles(ProductFiles model);
    }
}
