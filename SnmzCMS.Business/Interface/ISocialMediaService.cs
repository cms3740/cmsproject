﻿using System.Collections.Generic;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Interface
{
    public interface ISocialMediaService
    {
       
        List<SocialMedias> List();       
        ResultModel AddOrUpdate(SocialMedias model);           
        ResultModel Delete(int id);
        SocialMedias GetById(int id);        
    }
}
