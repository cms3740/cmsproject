﻿using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Interface
{
    public interface IFileService
    {
        ResultModel SaveFile(Files model);
    }
}
