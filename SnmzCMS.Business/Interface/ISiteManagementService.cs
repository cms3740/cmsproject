﻿using System.Collections.Generic;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Interface
{
    public interface ISiteManagementService
    {
       
        List<SiteManagement> List();       
        ResultModel AddOrUpdate(SiteManagement model);           
        ResultModel Delete(int id);
        SiteManagement GetById(int id);
        SiteManagement GetByName(string name);
    }
}
