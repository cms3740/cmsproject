﻿using System.Collections.Generic;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Interface
{
    public interface IDocumentGalleryService
    {
        ResultModel AddOrUpdate(DocumentGallery model);
        ResultModel Delete(int documentGalleryId);       
        List<DocumentGallery> GetList(int statusId);
        DocumentGallery GetById(int documentGalleryId);
        ResultModel AddDocumentGalleryFiles(DocumentGalleryFiles model);
    }
}
