﻿using System.Collections.Generic;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Interface
{
    public interface ICategoryService
    {
        ResultModel AddOrUpdate(Category model);
        ResultModel Delete(int categoryId);       
        List<Category> GetList(int statusId);
        Category GetById(int categoryId);
    }
}
