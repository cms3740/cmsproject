﻿using System.Collections.Generic;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Interface
{
    public interface IBannerService
    {
        ResultModel AddOrUpdate(Banners model);
        ResultModel Delete(int categoryId);
        List<Banners> GetList(int statusId);
        Banners GetById(int categoryId);
    }
}
