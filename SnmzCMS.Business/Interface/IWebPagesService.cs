﻿using System.Collections.Generic;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Interface
{
    public interface IWebPagesService
    {
        ResultModel AddOrUpdate(WebPages model);
        ResultModel Delete(int menuId);       
        List<WebPages> GetList(int statusId);
        WebPages GetById(int menuId);        
    }
}
