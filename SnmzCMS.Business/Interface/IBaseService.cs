﻿using SnmzCMS.Core.ViewModel;

namespace SnmzCMS.Business.Interface
{
    public interface IBaseService
    {
        ResultModel Result { get; set; }
    }
}
