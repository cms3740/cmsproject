﻿using System.Collections.Generic;
using SnmzCMS.Core.ViewModel;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Interface
{
    public interface IGalleryService
    {
        ResultModel AddOrUpdate(Gallery model);
        ResultModel Delete(int galleryId);       
        List<Gallery> GetList(int statusId);
        Gallery GetById(int galleryId);
        ResultModel AddGalleryFiles(GalleryFiles model);
    }
}
