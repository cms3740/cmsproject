﻿using SnmzCMS.Data;

namespace SnmzCMS.Business.Helper
{
    public class MediaHelper
    {
        public static string GetFileUrl(Files media)
        {
            if(media != null)
            {
                return media.ThumbPath ?? media.FolderPath;
            }
            else
            {
                return $@"{"/assets/images/hazirlaniyor.png"}";
            }
        }

    }
}
