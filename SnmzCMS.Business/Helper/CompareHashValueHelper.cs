﻿namespace SnmzCMS.Business.Helper
{
    public static class CompareHashValueHelper
    {
        public static bool CompareHashValue(string username, string password, string oldHashValue, byte[] salt)
        {
            try
            {
                string expectedHashString = Generator.Get_HASH_SHA512(username, password, salt);

                return (oldHashValue == expectedHashString);
            }
            catch
            {
                return false;
            }
        }
    }
}
