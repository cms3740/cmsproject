﻿using System;
using System.IO;
using System.Web;
using System.Web.Helpers;
using SnmzCMS.Business.Enums;
using SnmzCMS.Data;

namespace SnmzCMS.Business.Helper
{
    public class UploadHelper
    {
        /// <summary> </summary>
        /// <param name="imagePath">Burada fiziksel adresi alıyoruz ki dosyayı nerede olursa olsun,Yayınlandığı makine üzerinde ilgili yere kayıt edebilelim.</param>
        /// <param name="imageDirectory"> Uygulama hangi domainde çalışırsa çalışsın,dosyayı okumak için bu dizinden başlayacak. . </param>
        /// <param name="file">Yüklenecek dosya</param>
        /// <param name="fileType">imageDirectory nin altındaki alt klasörü belirtir.</param>
        /// <returns></returns>
        public static Files UploadFile(string imagePath,string imageDirectory,HttpPostedFileBase file, FileTypeEnum fileType)
        {
            if (file.ContentLength <= 0) return null;

            var fileName = Guid.NewGuid().ToString();

            var media = new Files
            {
                StatusId = (int) StatusEnum.Active,
                Extension = Path.GetExtension(file.FileName),
                Name = fileName,
                Size = file.ContentLength.ToString(),
                TypeId = (int) fileType,
                FullPath = $@"{imagePath}\{imageDirectory}\{Enum.GetName(typeof(FileTypeEnum), fileType)}\{fileName}{
                        Path.GetExtension(file.FileName)
                    }",
               FolderPath = $@"\{imageDirectory}\{Enum.GetName(typeof(FileTypeEnum), fileType)}\{fileName}{
                        Path.GetExtension(file.FileName)
                    }",

            };     
            file.SaveAs(media.FullPath);

            if (file.ContentType.Contains("image"))
            {

                //Küçük halini kaydet
                var thumbPath =
                    $@"\{imageDirectory}\{Enum.GetName(typeof(FileTypeEnum), fileType)}\{"t_" + fileName}{
                            Path.GetExtension(file.FileName)
                        }";

                var thumbPathSave =
                    $@"{imagePath}\{imageDirectory}\{Enum.GetName(typeof(FileTypeEnum), fileType)}\{"t_" + fileName}{
                            Path.GetExtension(file.FileName)
                        }";

                if (ThumbFile(file, thumbPathSave))
                    media.ThumbPath = thumbPath;
            }

            
            return media;
        }

        private  static bool ThumbFile(HttpPostedFileBase file,string path)
        {
             var img = new WebImage(file.InputStream);
            if (img.Width <= 800) return false;
            img.Resize(400, 300);
            img.Save(path);
            return true;

        }

    }
}
