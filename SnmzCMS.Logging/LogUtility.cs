﻿using System;
using System.Net;
using System.Net.Mail;
using NLog;
using NLog.Config;
using NLog.Targets;


namespace SnmzCMS.Logging
{
    public class LogUtility
    {

        //public static string BuildExceptionMessage(Exception x)
        //{
        //    var logException = x;
        //    if (x.InnerException != null)
        //        logException = x.InnerException;



        //    #region DbConnection
        //    var dbTarget = new DatabaseTarget
        //    {

        //        Name = "database",                  
        //        ConnectionString = "data source=94.73.170.2;initial catalog=u5319206_snmz;persist security info=True;user id=u5319206_snmz;password=UMqs49A0;MultipleActiveResultSets=True;App=EntityFramework",
        //        CommandType = System.Data.CommandType.Text,
        //        CommandText =
        //            "insert into Log (Logged, Level, Message,ServerName,Url,Logger, CallSite, Exception,IpAdress) values(@Logged, @Level, @Message,@ServerName, @Url,@Logger, @Callsite, @Exception,@IpAdress); "
        //    };
        //    #endregion

        //    #region Parameters
        //    var param = new DatabaseParameterInfo
        //    {
        //        Name = "@logged",
        //        Layout = "${date}"
        //    };
        //    dbTarget.Parameters.Add(param);

        //    param = new DatabaseParameterInfo
        //    {
        //        Name = "@level",
        //        Layout = "${level}"
        //    };
        //    dbTarget.Parameters.Add(param);

        //    param = new DatabaseParameterInfo
        //    {
        //        Name = "@message",
        //        Layout = "${message}"
        //    };
        //    dbTarget.Parameters.Add(param);

        //    param = new DatabaseParameterInfo
        //    {
        //        Name = "@serverName",
        //        Layout = ""
        //    };
        //    dbTarget.Parameters.Add(param);

        //    param = new DatabaseParameterInfo
        //    {
        //        Name = "@url",
        //        Layout = ""
        //    };
        //    dbTarget.Parameters.Add(param);

        //    param = new DatabaseParameterInfo
        //    {
        //        Name = "@logger",
        //        Layout = "${logger}"
        //    };
        //    dbTarget.Parameters.Add(param);

        //    param = new DatabaseParameterInfo
        //    {
        //        Name = "@callSite",
        //        Layout = "${callsite}"
        //    };
        //    dbTarget.Parameters.Add(param);

        //    param = new DatabaseParameterInfo
        //    {
        //        Name = "@exception",
        //        Layout = "${exception:tostring}"
        //    };
        //    dbTarget.Parameters.Add(param);

        //    param = new DatabaseParameterInfo
        //    {
        //        Name = "@IpAdress",
        //        Layout = ""
        //    };
        //    dbTarget.Parameters.Add(param);
        //    #endregion

        //    //Hem DB hemde mail atması için her iki rolü de bu şekilde çalıştırmamız gerekiyor.
        //    //LogManager.Configuration.AddTarget(mailTarget);
        //    //LogManager.Configuration.AddTarget(dbTarget);
        //    //// var r1 = new LoggingRule("*", LogLevel.Debug, mailTarget);
        //    //var r2 = new LoggingRule("*", LogLevel.Debug, dbTarget);
        //    ////LogManager.Configuration.LoggingRules.Add(r1);
        //    //LogManager.Configuration.LoggingRules.Add(r2);
        //    //LogManager.Configuration.Reload();


        //    SimpleConfigurator.ConfigureForTargetLogging(dbTarget, LogLevel.Debug);


        //    #region SendMail
        //    // Get the error message

        //    var strErrorMsg = Environment.NewLine + "Message :" + logException.Message;
        //    strErrorMsg += Environment.NewLine + "Source :" + logException.Source;
        //    strErrorMsg += Environment.NewLine + "Stack Trace :" + logException.StackTrace;
        //    strErrorMsg += Environment.NewLine + "TargetSite :" + logException.TargetSite;

        //    SmtpClient sc = new SmtpClient
        //    {
        //        Port = 587,
        //        Host = "smtp.gmail.com",
        //        EnableSsl = true,
        //        Timeout = 50000,
        //        Credentials = new NetworkCredential("snzmcms@gmail.com", "Hpw1907$$")
        //    };

        //    var mail = new MailMessage { From = new MailAddress("snzmcms@gmail.com", "CMS Admin") };

        //    mail.To.Add("ahmet.sonmez37@gmail.com");
        //    mail.Subject = "CMS Admin Hata Var";
        //    mail.IsBodyHtml = true;
        //    mail.Body = strErrorMsg;

        //    sc.Send(mail);

        //    #endregion

        //    return strErrorMsg;

        //}
        public static string BuildExceptionMessage(Exception x)
        {

            Exception logException = x;
            if (x.InnerException != null)
                logException = x.InnerException;

            // Get the error message
            string strErrorMsg = Environment.NewLine + "Message :" + logException.Message;

            // Source of the message
            strErrorMsg += Environment.NewLine + "Source :" + logException.Source;

            // Stack Trace of the error

            strErrorMsg += Environment.NewLine + "Stack Trace :" + logException.StackTrace;

            // Method where the error occurred
            strErrorMsg += Environment.NewLine + "TargetSite :" + logException.TargetSite;
            return strErrorMsg;
        }
    }
}
