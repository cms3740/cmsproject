﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnmzCMS.Data.ViewModel
{
    public class RegisterViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public byte[] Salt { get; set; }
        public string Hash { get; set; }
        public int RoleId { get; set; }
        public int StatusId { get; set; }
        public string Password { get; set; }
    }
}
