﻿using System;

namespace SnmzCMS.Core.ViewModel
{
    public class ResultModel
    {
        public int Id { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }

        public ResultModel()
        {
            Success = true;
        }
    }

    public class ResultModel<T> : ResultModel
    {
        public T Data { get; set; }
    }
}
