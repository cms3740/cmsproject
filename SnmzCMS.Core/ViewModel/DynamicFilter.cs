﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SnmzCMS.Core.ViewModel
{
    public class DynamicFilter
    {
        public string FilterText { get; set; }
        public object[] Values { get; set; }

        public DynamicFilter()
        {
        }

        public DynamicFilter(Dictionary<string, string> searchCriterias, Type entityType)
        {
            Values = new object[searchCriterias.Count];

            int objectFilter = 0;
            for (int i = 0; i < searchCriterias.Keys.Count; i++)
            {
                if (!string.IsNullOrWhiteSpace(FilterText))
                    FilterText += " and ";

                string key = searchCriterias.Keys.Skip(i).Take(1).First();
                string value = searchCriterias.Values.Skip(i).Take(1).First();

                PropertyInfo propertyInfo = entityType.GetProperty(key);
                string typeName = "System.String";

                if (propertyInfo != null)
                    typeName = propertyInfo.PropertyType.Name;

                if (typeName.Contains("Int")
                    || typeName.Contains("Nullable"))
                {
                    FilterText += $"{key}={int.Parse(value)}";
                    Values[i] = int.Parse(value);
                    objectFilter++;
                }
                else if (typeName.Contains("Guid"))
                {
                    FilterText += $"{key}.Equals(@{objectFilter})";
                    Values[i] = new Guid(value);
                    objectFilter++;
                }
                else if (typeName.Contains("Boolean"))
                {
                    FilterText += $"{key}.Equals(@{objectFilter})";
                    Values[i] = Boolean.Parse(value);
                    objectFilter++;
                }
                else if (typeName.Contains("DateTime"))
                {
                    FilterText += string.Format("it.{0} >= @{1} and it.{0} <= Convert.ToDateTime(@{1}).AddDays(1)", key, objectFilter);
                    Values[i] = DateTime.Parse(value);
                    objectFilter++;
                }
                else
                {
                    FilterText += $"{key}.Contains(@{objectFilter})";
                    Values[i] = value;
                    objectFilter++;
                }

            }
        }
    }
}
