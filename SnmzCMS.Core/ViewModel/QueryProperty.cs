﻿using System.Collections.Generic;

namespace SnmzCMS.Core.ViewModel
{
    public class QueryProperty
    {
        public Dictionary<string, string> SearchCriterias { get; set; }
        public string OrderByString { get; set; }
    }
}
