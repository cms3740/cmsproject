﻿using System;
using System.Transactions;

namespace SnmzCMS.Core
{
    public interface IUnitOfWork<TC> : IDisposable
    {
        int Commit();
        TC GetContext { get; set; }
        void Refresh();
        TransactionScope BeginTransaction();
    }
}
