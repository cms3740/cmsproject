﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using SnmzCMS.Core.ViewModel;

namespace SnmzCMS.Core
{
    public class GRepository<TEntity, TC>
      where TEntity : class
      where TC : DbContext
    {
        #region Constructure

        public GRepository(IUnitOfWork<TC> unitOfWork)
        {
            UnitOfWork = unitOfWork;

            DbSet = UnitOfWork.GetContext.Set<TEntity>();
        }

        #endregion

        #region Variable

        public DbSet<TEntity> DbSet;

        public Type EntityType => typeof(TEntity);

        protected IUnitOfWork<TC> UnitOfWork { get; private set; }

        public virtual Expression<Func<TEntity, bool>> DefaultFilter => null;

        public virtual Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> DefaultSorter => null;

        public IQueryable<TEntity> Source => DbSet.AsQueryable();

        #endregion

        #region Crud

        public virtual void Insert(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public virtual void InsertColletion(List<TEntity> entityList)
        {
            foreach (var entity in entityList)
            {
                DbSet.Add(entity);
            }
        }

        public List<TEntity> GetList()
        {

            var query = DbSet.ToList();
            return query;
        }

        public TEntity GetById(int id)
        {
            return DbSet.Find(id);

        }

        public virtual void Delete(object id, bool forceDelete = false)
        {
            TEntity entityToDelete = DbSet.Find(id);
            if (forceDelete)
                Delete(entityToDelete);
            else
            {
                if (entityToDelete != null && entityToDelete.GetType().GetProperty("StatusId") != null)
                {
                    entityToDelete.GetType().GetProperty("StatusId")?.SetValue(entityToDelete, -1, null);
                    Update(entityToDelete);
                }
            }
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (UnitOfWork.GetContext.Entry(entityToDelete).State == EntityState.Detached)
            {
                DbSet.Attach(entityToDelete);
            }
            DbSet.Remove(entityToDelete);
        }

        public virtual void DeleteAll(IEnumerable<TEntity> entities)
        {
            DbSet.RemoveRange(entities);
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            DbSet.Attach(entityToUpdate);
            UnitOfWork.GetContext.Entry(entityToUpdate).State = EntityState.Modified;

        }

        public virtual ResultModel Commit()
        {
            var result = new ResultModel { Success = true };
            try
            {
                UnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Exception = ex;
                result.Message = ex.Message;
            }
            return result;
        }

        #endregion

        #region Dispose

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {

            if (!_disposed)
                if (disposing)
                    UnitOfWork.GetContext.Dispose();

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion


    }
}
