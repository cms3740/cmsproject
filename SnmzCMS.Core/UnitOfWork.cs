﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;

namespace SnmzCMS.Core
{
    public class UnitOfWork<TC> : IUnitOfWork<TC> where TC : DbContext
    {
        private bool _disposed;
        private readonly TC _dbContext = null;
        private TransactionScope _transaction;

        public UnitOfWork()
        {
            GetContext = _dbContext ?? Activator.CreateInstance<TC>();
        }

        public int Commit()
        {
            try
            {
                return GetContext.SaveChanges();
            }
            catch (DbEntityValidationException)
            {
            }
            catch (Exception)
            {
                // ignored
            }
            return 0;
        }


        public TC GetContext
        {
            get;
            set;
        }
        public void Refresh()
        {
            var context = ((IObjectContextAdapter)GetContext).ObjectContext;
            var refreshableObjects = (from entry in context.ObjectStateManager.GetObjectStateEntries(
                                                        EntityState.Added
                                                       | EntityState.Deleted
                                                       | EntityState.Modified
                                                       | EntityState.Unchanged)
                                      where entry.EntityKey != null
                                      select entry.Entity).ToList();

            context.Refresh(RefreshMode.StoreWins, refreshableObjects);
        }

        public TransactionScope BeginTransaction()
        {
            if (null != _transaction)
            {
                _transaction = new TransactionScope();
            }

            return _transaction;
        }

        #region IDisposable Members
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (null != _transaction)
                    {
                        //_transaction.Dispose();
                    }

                    if (null != _dbContext)
                    {
                        _dbContext.Dispose();
                    }

                }

            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
